scalaVersion := "2.13.0"

val http4sVersion = "0.21.0-SNAPSHOT"

// Only necessary for SNAPSHOT releases
resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "dev.zio" %% "zio" % "1.0.0-RC11-1",
  "dev.zio" %% "zio-test" % "1.0.0-RC11-1",
  "dev.zio" %% "zio-interop-cats" % "2.0.0.0-RC2"
)